#include <Adafruit_ILI9341.h>

#include <gfxfont.h>
#include <Adafruit_GFX.h>

#include "SPI.h"

void (* resetFunc) (void) = 0; //Used to reset the Arduino.

//For debugging + testing
#define DBG

// For the Adafruit shield, these are the default.
#define TFT_DC 9
#define TFT_CS 10
Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC);
//ILI9341 leaves digital pins 2-7 available for use (4 only if SD card reader not being used). 

//Arduino Input/Ouput pin IDs
//Analogue
const int JOYSTICK_X = 4;
const int JOYSTICK_Y = 5;
//Digital
const int JOYSTICK_PRESS = 2;
const int BUTTON_A = 3;
const int BUTTON_B = 4;

//Screen info
const int SCREEN_WIDTH = tft.width();
const int SCREEN_HEIGHT = tft.height();
#define FPS 25
#define SPF (1/FPS)*1000000
int backgroundColour;
#define CLEAR_PADDING 1

//Player info
#define PLAYER_WIDTH 11
#define PLAYER_HEIGHT 8
#define SHOT_SPEED 250000 //Micro seconds between shots

uint16_t playerSprite[PLAYER_HEIGHT][PLAYER_WIDTH] = {
#define B ILI9341_BLACK
#define W ILI9341_WHITE
  {B,B,B,B,B,W,B,B,B,B,B},
  {W,B,B,B,W,W,W,B,B,B,W},
  {W,B,B,W,W,W,W,W,B,B,W},
  {W,W,W,W,W,W,W,W,W,W,W},
  {W,W,W,W,W,W,W,W,W,W,W},
  {W,B,W,W,W,W,W,W,W,B,W},
  {W,B,W,W,W,W,W,W,W,B,W},
  {B,B,B,W,W,B,W,W,B,B,B}
#undef B
#undef W
};

struct Player
{
  const float START_X = SCREEN_WIDTH/2;
  const float START_Y = SCREEN_HEIGHT*0.79f;
  float x, y, prevX, prevY, speed;
  unsigned long timeLastShot;
}player;

//Bullet info
#define BUL_WIDTH 1
#define BUL_HEIGHT 5
#define BUL_POOL_SIZE 20
#define BUL_SPEED 500000
int numActiveBullets = 0;
struct Bullet
{
  float x, y, prevX, prevY;
  bool active;
  uint16_t timeShot;
}bulletPool[BUL_POOL_SIZE];
uint16_t bulletSprite[BUL_HEIGHT][BUL_WIDTH] = {
  #define B ILI9341_GREEN
  {B},
  {B},
  {B},
  {B},
  {B}
  #undef B
};

//Invader info
#define INV_WIDTH 11
#define INV_HEIGHT 8

uint16_t invaderSprite[INV_HEIGHT][INV_WIDTH] = {
#define B ILI9341_BLACK
#define W ILI9341_WHITE
  {B,B,W,B,B,B,B,B,W,B,B},
  {B,B,B,W,B,B,B,W,B,B,B},
  {B,B,W,W,W,W,W,W,W,B,B},
  {B,W,W,B,W,W,W,B,W,W,B},
  {W,W,W,W,W,W,W,W,W,W,W},
  {W,B,W,W,W,W,W,W,W,B,W},
  {W,B,W,B,B,B,B,B,W,B,W},
  {B,B,B,W,W,B,W,W,B,B,B}
#undef B
#undef W
};

#define MAX_INVADERS 24
#define INVADERS_P_WAVE 12
#define CHARGE_SPEED 250000
#define WAVE_ROWS 4
#define WAVE_COLS 3
struct Invader
{
  bool alive = true;
  float x, y, prevX, prevY, speedy;
} invaders[MAX_INVADERS];

//Level info
struct Level
{
   int numWaves, currentWave, backgroundColour;
   unsigned long timeWaveStarted, timeBetweenWaves;
}level;

//Game info
struct Game
{
  int score;
  unsigned long previousFrameTime = 0, previousUpdateTime;
  bool charging = false;
}game;

//Input info
const float JOYSTICK_RANGE = 1023.0f;
const float JOYSTICK_TRESHOLD = 0.1;
struct Button
{
  int onDown, down, onUp;
};
struct Input
{
  float x, y;
  int stickPressed;
  Button buttonA, buttonB;
}input;

void setup() 
{  
  //Setup screen
  tft.begin();

  //Setup input pins
  pinMode(BUTTON_A, INPUT);
  pinMode(BUTTON_B, INPUT);
  pinMode(JOYSTICK_PRESS,INPUT);
  digitalWrite(JOYSTICK_PRESS,HIGH); //Turn on pull-up resistor

#ifdef DBG
  //Setup serial output for debugging
  Serial.begin(9600);
  uint8_t x = tft.readcommand8(ILI9341_RDMODE);
  Serial.print("Display Power Mode: 0x"); Serial.println(x, HEX);
  x = tft.readcommand8(ILI9341_RDMADCTL);
  Serial.print("MADCTL Mode: 0x"); Serial.println(x, HEX);
  x = tft.readcommand8(ILI9341_RDPIXFMT);
  Serial.print("Pixel Format: 0x"); Serial.println(x, HEX);
  x = tft.readcommand8(ILI9341_RDIMGFMT);
  Serial.print("Image Format: 0x"); Serial.println(x, HEX);
  x = tft.readcommand8(ILI9341_RDSELFDIAG);
  Serial.print("Self Diagnostic: 0x"); Serial.println(x, HEX); 
#endif

  //Initialise player
  player.x = player.START_X;
  player.y = player.START_Y;
  player.timeLastShot = micros();
  player.speed = 300000;
  
  //Initialise game
  game.previousFrameTime = micros();
  game.score = 0;
  
  //Initialise level
  level.numWaves = 10;
  level.currentWave = 0;
  level.timeBetweenWaves = 10000000;
  level.timeWaveStarted = micros();
  level.backgroundColour = ILI9341_BLACK;

  setupInvaders();


  tft.fillScreen(backgroundColour);
}


void loop(void) 
{
  unsigned long deltaTime = micros() - game.previousUpdateTime;
  
  getInput();
  update(deltaTime);
  draw();
  
  game.previousUpdateTime = micros();
}

void update(unsigned long deltaTime)
{
  float dtf = deltaTime/1000000.0f;

  float mag = magnitude(input.x, input.y);
  if(mag >= JOYSTICK_TRESHOLD)
  {
    player.x += input.x / mag * player.speed * dtf;
    player.y -= input.y / mag * player.speed * dtf;
    
    if(player.x < CLEAR_PADDING) player.x = CLEAR_PADDING;
    if(player.y < CLEAR_PADDING) player.y = CLEAR_PADDING;
    if(player.x > SCREEN_WIDTH - PLAYER_WIDTH - CLEAR_PADDING) player.x = SCREEN_WIDTH - PLAYER_WIDTH - CLEAR_PADDING;
    if(player.y > SCREEN_HEIGHT - PLAYER_HEIGHT - CLEAR_PADDING) player.y = SCREEN_HEIGHT - PLAYER_HEIGHT - CLEAR_PADDING;
  }

  if(input.buttonA.onDown || input.buttonA.down)
  {
    if(micros() - player.timeLastShot > SHOT_SPEED)
    {
      shoot();
      player.timeLastShot = micros();
    }
  }

  updateBullets(dtf);
  updateInvaders(dtf);
  handleCollisions();
  updateLevel();
}

void invaderShot(Invader &inv, Bullet &bullet)
{
  inv.alive = false;
  bullet.active = false;
}

void playerCrashed()
{
  resetFunc();
}

void handleCollisions()
{
  for(int i=0; i<MAX_INVADERS; ++i)
  {
    if(invaders[i].alive)
    { 
      //Invader to bullets
      for(int b=0; b<BUL_POOL_SIZE; ++b)
      {
        if(bulletPool[b].active)
        {
          if(bulletPool[b].x > invaders[i].x && bulletPool[b].x + BUL_WIDTH < invaders[i].x + INV_WIDTH)
          {
            if(bulletPool[b].y > invaders[i].y && bulletPool[b].y + BUL_HEIGHT < invaders[i].y + INV_HEIGHT)
            {
              invaderShot(invaders[i], bulletPool[b]);
            }
          }
        }
      }
  
      //Invader to player
      if(player.x > invaders[i].x && player.x + PLAYER_WIDTH < invaders[i].x + INV_WIDTH)
      {
        if(player.y > invaders[i].y && player.y + PLAYER_WIDTH < invaders[i].y + INV_HEIGHT)
        {
          playerCrashed();
        }
      }
    }
  }
}

void oscillateInvaders(float dtf)
{
  int offset = level.currentWave%2 * INVADERS_P_WAVE;
  float time = (millis()/1000.0f)*3.14f;
  for(int r=0; r<WAVE_ROWS; ++r)
  {
    for(int c=0; c<WAVE_COLS; ++c)
    {
      int index = c + r*WAVE_COLS;
    
      if(r%2==0)
      {
        if(invaders[index + offset].alive)
          invaders[index + offset].x += sin(time);
        if(game.charging && invaders[index + ((offset-INVADERS_P_WAVE)*-1)].alive)
        {
          invaders[index + ((offset-INVADERS_P_WAVE)*-1)].x += sin(time);
        }
      }
      else
      {
        if(invaders[index + offset].alive)
          invaders[index + offset].x -= sin(time);
        if(game.charging && invaders[index + ((offset-INVADERS_P_WAVE)*-1)].alive)
        {
          invaders[index + ((offset-INVADERS_P_WAVE)*-1)].x -= sin(time);
        }
      }
    }
  }
}

void charge()
{
  game.charging = true;
  int offset = level.currentWave%2 * INVADERS_P_WAVE;
  for(int i=offset; i<INVADERS_P_WAVE + offset; ++i)
  {
    if(invaders[i].alive)
      invaders[i].speedy = CHARGE_SPEED;
  }
}

void updateInvaders(float dtf)
{
  oscillateInvaders(dtf);
  if(game.charging)
  {
    for(int i=0; i<MAX_INVADERS; ++i)
    {
      if(invaders[i].alive)
        invaders[i].y += invaders[i].speedy * dtf;
    }
  }
  else
  {
    int offset = level.currentWave%2 * INVADERS_P_WAVE;
    for(int i=offset; i<INVADERS_P_WAVE + offset; ++i)
    {
      if(invaders[i].alive)
        invaders[i].y += invaders[i].speedy * dtf;
    }
  }
}

void updateLevel()
{
  if(micros() - level.timeWaveStarted > level.timeBetweenWaves)
  {
    charge();
    level.currentWave++;
    level.timeWaveStarted = micros();
    if(level.currentWave == level.numWaves)
    {
      generateLevel();
    }
    setupInvaders();
  }
}

void updateBullets(float dtf)
{
  for(int i=0; i<BUL_POOL_SIZE; ++i)
  {
    if(bulletPool[i].active)
    {
      bulletPool[i].y -= BUL_SPEED * dtf;
      if( bulletPool[i].x < CLEAR_PADDING || bulletPool[i].y < CLEAR_PADDING ||
          bulletPool[i].x > SCREEN_WIDTH - BUL_WIDTH - CLEAR_PADDING ||
          bulletPool[i].y > SCREEN_HEIGHT - BUL_HEIGHT - CLEAR_PADDING)
          {
            bulletPool[i].active = false;
          }
    }
  }
}

void shoot()
{ 
  int oldestIndex = -1;
  for(int i=0; i<BUL_POOL_SIZE; ++i)
  {
    if(!bulletPool[i].active)
    {
      bulletPool[i].x = player.x + PLAYER_WIDTH/2;
      bulletPool[i].y = player.y + PLAYER_HEIGHT/2;
      bulletPool[i].timeShot = micros();
      bulletPool[i].active = true;
      return;
    }
    if(bulletPool[i].timeShot < bulletPool[oldestIndex].timeShot)
      oldestIndex = i;
  }
  bulletPool[oldestIndex].x = player.x;
  bulletPool[oldestIndex].y = player.y;
  bulletPool[oldestIndex].timeShot = micros();
  bulletPool[oldestIndex].active = true;
}

void draw()
{
  if(micros() - game.previousFrameTime > SPF)
  {
    drawInvaders();
    drawPlayer(player);
    drawBullets();
  
    game.previousFrameTime = micros();
  }
}

void getInput()
{
  //General buttons
  handleButtonInput(input.buttonA, digitalRead(BUTTON_A));
  handleButtonInput(input.buttonB, digitalRead(BUTTON_B));
  
  //Joystick
  input.x = analogRead(JOYSTICK_X)/JOYSTICK_RANGE*2 - 1.0f;
  input.y = analogRead(JOYSTICK_Y)/JOYSTICK_RANGE*2 - 1.0f;
  input.stickPressed = (digitalRead(JOYSTICK_PRESS) == LOW) ? true : false;
#ifdef DBGI
  Serial.print("x = "); Serial.print(input.x, DEC); 
  Serial.print("; y = "); Serial.print(input.y, DEC); 
  Serial.print("; pushed = "); Serial.println((input.stickPressed == LOW) ? "true" : "false"); 
  Serial.print("A = "); 
  if(input.buttonA.onDown == HIGH)
  {
    Serial.print("On Down; "); 
  }
  if(input.buttonA.down == HIGH)
  {
    Serial.print("Down; "); 
  }
  if(input.buttonA.onUp == HIGH)
  {
    Serial.print("On Up; "); 
  }
  Serial.println();
  Serial.print("B = "); 
  if(input.buttonB.onDown == HIGH)
  {
    Serial.print("On Down; "); 
  }
  if(input.buttonB.down == HIGH)
  {
    Serial.print("Down; "); 
  }
  if(input.buttonB.onUp == HIGH)
  {
    Serial.print("On Up; "); 
  }
  Serial.println();
#endif
}

void handleButtonInput(Button &button, int input)
{
  if(input == HIGH)//Pressed
  {
    if(button.onDown == true)
    {
      button.onDown = false;
      button.down = true;
    }
    else if(button.down == false)
    {
      button.onDown = true;
    }
  }
  else //Not pressed
  {
    if(button.onDown == true || button.down == true)
    {
      button.onUp = true;
      button.onDown = false;
      button.down = false;
    }
    else if(button.onUp == true)
    {
      button.onUp = false;
    }
  }
}

void drawInvaders()
{
  int offset = level.currentWave%2 * INVADERS_P_WAVE;
  if(game.charging)
  {
    for(int i=0; i<MAX_INVADERS; ++i)
    {
      if(invaders[i].alive)
        tft.fillRect(invaders[i].prevX-CLEAR_PADDING, invaders[i].prevY-CLEAR_PADDING, INV_WIDTH+CLEAR_PADDING*2, INV_HEIGHT+CLEAR_PADDING*2, level.backgroundColour);
        invaders[i].prevX = invaders[i].x; invaders[i].prevY = invaders[i].y;
        tft.drawSprite<INV_WIDTH, INV_HEIGHT>(invaderSprite, (uint16_t)invaders[i].x, (uint16_t)invaders[i].y);
    }
  }
  else
  {
    for(int i=offset; i<INVADERS_P_WAVE+offset; ++i)
    {
      if(invaders[i].alive)
        tft.fillRect(invaders[i].prevX-CLEAR_PADDING, invaders[i].prevY-CLEAR_PADDING, INV_WIDTH+CLEAR_PADDING*2, INV_HEIGHT+CLEAR_PADDING*2, level.backgroundColour);
        invaders[i].prevX = invaders[i].x; invaders[i].prevY = invaders[i].y;
        tft.drawSprite<INV_WIDTH, INV_HEIGHT>(invaderSprite, (uint16_t)invaders[i].x, (uint16_t)invaders[i].y);
    }
  }
}

void drawPlayer(Player &player)
{
  tft.fillRect(player.prevX-CLEAR_PADDING, player.prevY-CLEAR_PADDING, PLAYER_WIDTH+CLEAR_PADDING*2, PLAYER_HEIGHT+CLEAR_PADDING*2, level.backgroundColour);
  player.prevX = player.x; player.prevY = player.y;
  tft.drawSprite<PLAYER_WIDTH, PLAYER_HEIGHT>(playerSprite, player.x, player.y);
}

void drawBullets()
{
  unsigned long begin = micros();
  for(int i=0; i<BUL_POOL_SIZE; ++i)
  {
    if(bulletPool[i].active)
    {
    tft.fillRect(bulletPool[i].prevX-CLEAR_PADDING, bulletPool[i].prevY-CLEAR_PADDING, BUL_WIDTH+CLEAR_PADDING*2, BUL_HEIGHT+CLEAR_PADDING*2, level.backgroundColour);
    bulletPool[i].prevX = bulletPool[i].x; bulletPool[i].prevY = bulletPool[i].y;
    tft.drawSprite<BUL_WIDTH, BUL_HEIGHT>(bulletSprite, bulletPool[i].x, bulletPool[i].y);
    }
  }
  unsigned long diff = micros() - begin;
//  Serial.print("Time to draw bullets: ");
//  Serial.print(diff/1000.0f, DEC);
//  Serial.println("ms");
}
float magnitude(float x, float y)
{
  return sqrt(x*x+y*y);
}

void generateLevel()
{
  if(level.timeBetweenWaves > 3)
  {
    level.timeBetweenWaves -= 1000000;
  }
  level.numWaves++;
  level.currentWave = 0;
}

void setupInvaders()
{
  int offset = level.currentWave%2 * INVADERS_P_WAVE;

  for(int r=0; r<WAVE_ROWS; ++r)
  {
    for(int c=0; c<WAVE_COLS; ++c)
    {
      int index = c + r*WAVE_COLS;
      invaders[index+offset].x = SCREEN_WIDTH/6.0f + SCREEN_WIDTH*0.9f/WAVE_COLS*c;
      invaders[index+offset].y = SCREEN_HEIGHT/3.0f + SCREEN_HEIGHT*0.33f/WAVE_ROWS*r;
      invaders[index+offset].speedy = 0;
      invaders[index+offset].alive = true;
    }
  }
}



