#include <Adafruit_ILI9341.h>

#include <gfxfont.h>
#include <Adafruit_GFX.h>

#include "SPI.h"


//For debugging + testing
#define DBG

// For the Adafruit shield, these are the default.
#define TFT_DC 9
#define TFT_CS 10
Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC);
//ILI9341 leaves digital pins 2-7 available for use (4 only if SD card reader not being used). 

//Arduino Input/Ouput pin IDs
//Analogue
const int JOYSTICK_X = 4;
const int JOYSTICK_Y = 5;
//Digital
const int JOYSTICK_PRESS = 2;
const int BUTTON_A = 3;
const int BUTTON_B = 4;

//Screen info
const int SCREEN_WIDTH = tft.width();
const int SCREEN_HEIGHT = tft.height();
int backgroundColour;

//Player info
struct Player
{
  int x, y, timeLastShot;
}player;

//Invader info
#define INV_WIDTH 11
#define INV_HEIGHT 8
int invX, invY, invColour;

int invaderSprite[INV_HEIGHT][INV_WIDTH] = {
  {0,0,1,0,0,0,0,0,1,0,0},
  {0,0,0,1,0,0,0,1,0,0,0},
  {0,0,1,1,1,1,1,1,1,0,0},
  {0,1,1,0,1,1,1,0,1,1,0},
  {1,1,1,1,1,1,1,1,1,1,1},
  {1,0,1,1,1,1,1,1,1,0,1},
  {1,0,1,0,0,0,0,0,1,0,1},
  {0,0,0,1,1,0,1,1,0,0,0}
};
#define MAX_INVADERS 100
struct Invader
{
 int x, y;
 int colour;
} invaders[MAX_INVADERS];

//Level info
struct Level
{
   int numWaves, currentWave, timeBetweenWaves, timeSinceLastWave, backgroundColour;
}level;

//Game info
struct Game
{
  int score;
  unsigned long previousFrameTime = 0, previousUpdateTime;
}game;

//Input info
const float JOYSTICK_RANGE = 1023.0f;
struct Button
{
  int onDown, down, onUp;
};
struct Input
{
  float x, y;
  int stickPressed;
  Button buttonA, buttonB;
}input;

void setup() 
{  
  //Setup screen
  tft.begin();

  //Setup input pins
  pinMode(BUTTON_A, INPUT);
  pinMode(BUTTON_B, INPUT);
  pinMode(JOYSTICK_PRESS,INPUT);
  digitalWrite(JOYSTICK_PRESS,HIGH); //Turn on pull-up resistor

#ifdef DBG
  //Setup serial output for debugging
  Serial.begin(9600);
  uint8_t x = tft.readcommand8(ILI9341_RDMODE);
  Serial.print("Display Power Mode: 0x"); Serial.println(x, HEX);
  x = tft.readcommand8(ILI9341_RDMADCTL);
  Serial.print("MADCTL Mode: 0x"); Serial.println(x, HEX);
  x = tft.readcommand8(ILI9341_RDPIXFMT);
  Serial.print("Pixel Format: 0x"); Serial.println(x, HEX);
  x = tft.readcommand8(ILI9341_RDIMGFMT);
  Serial.print("Image Format: 0x"); Serial.println(x, HEX);
  x = tft.readcommand8(ILI9341_RDSELFDIAG);
  Serial.print("Self Diagnostic: 0x"); Serial.println(x, HEX); 
#endif

  //Initialise invaders to default values
  for(int i=0; i<MAX_INVADERS; ++i)
  {
    invaders[i].x = 0;
    invaders[i].y = 0;
    invaders[i].colour = ILI9341_WHITE;
  }

  //Initialise player
  player.x = 0;
  player.y = 0;
  player.timeLastShot = millis();
  
  //Initialise game
  game.previousFrameTime = millis();
  game.score = 0;
  
  //Initialise level
  level.numWaves = 1;
  level.currentWave = 1;
  level.timeBetweenWaves = 15;
  level.timeSinceLastWave = millis();
  level.backgroundColour = ILI9341_BLACK;


  tft.fillScreen(backgroundColour);
}


void loop(void) 
{
  unsigned long deltaTime = millis() - game.previousUpdateTime;
  getInput();
  update(deltaTime);
  if(millis() - game.previousFrameTime > 2000)//33)
  {
    draw();
  }
  game.previousUpdateTime = millis();
}

void update(unsigned long deltaTime)
{
  float dtf = deltaTime/1000.0f;
}

void draw()
{
  undrawInvader(invaders[0]);
  
  invaders[0].x = random(0, SCREEN_WIDTH - INV_WIDTH);
  invaders[0].y = random(0, SCREEN_HEIGHT - INV_HEIGHT);

  drawInvader(invaders[0]);

  game.previousFrameTime = millis();
}

void getInput()
{
  //General buttons
  handleButtonInput(input.buttonA, digitalRead(BUTTON_A));
  handleButtonInput(input.buttonB, digitalRead(BUTTON_B));
  
  //Joystick
  input.x = analogRead(JOYSTICK_X)/JOYSTICK_RANGE*2 - 1.0f;
  input.y = analogRead(JOYSTICK_Y)/JOYSTICK_RANGE*2 - 1.0f;
  input.stickPressed = (digitalRead(JOYSTICK_PRESS) == LOW) ? true : false;
#ifdef DBG
  Serial.print("x = "); Serial.print(input.x, DEC); 
  Serial.print("; y = "); Serial.print(input.y, DEC); 
  Serial.print("; pushed = "); Serial.println((input.stickPressed == LOW) ? "true" : "false"); 
  Serial.print("A = "); 
  if(input.buttonA.onDown == HIGH)
  {
    Serial.print("On Down; "); 
  }
  if(input.buttonA.down == HIGH)
  {
    Serial.print("Down; "); 
  }
  if(input.buttonA.onUp == HIGH)
  {
    Serial.print("On Up; "); 
  }
  Serial.println();
  Serial.print("B = "); 
  if(input.buttonB.onDown == HIGH)
  {
    Serial.print("On Down; "); 
  }
  if(input.buttonB.down == HIGH)
  {
    Serial.print("Down; "); 
  }
  if(input.buttonB.onUp == HIGH)
  {
    Serial.print("On Up; "); 
  }
  Serial.println();
#endif
}

void handleButtonInput(Button &button, int input)
{
  if(input == HIGH)//Pressed
  {
    if(button.onDown == true)
    {
      button.onDown = false;
      button.down = true;
    }
    else if(button.down == false)
    {
      button.onDown = true;
    }
  }
  else //Not pressed
  {
    if(button.onDown == true || button.down == true)
    {
      button.onUp = true;
      button.onDown = false;
      button.down = false;
    }
    else if(button.onUp == true)
    {
      button.onUp = false;
    }
  }
}

void drawInvader(const Invader &invader)
{
  for(int i=0; i<INV_WIDTH; ++i)
  {
    for(int j=0; j<INV_HEIGHT; ++j)
    {
      if(invaderSprite[j][i] == 1)
      {
        tft.drawPixel(invader.x+i, invader.y+j, invader.colour);
      }
    }
  }
}
void undrawInvader(const Invader &invader)
{
  for(int i=0; i<INV_WIDTH; ++i)
  {
    for(int j=0; j<INV_HEIGHT; ++j)
    {
      if(invaderSprite[j][i] == 1)
      {
        tft.drawPixel(invader.x+i, invader.y+j, level.backgroundColour);
      }
    }
  }
}
