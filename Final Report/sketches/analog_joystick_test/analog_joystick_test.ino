
const float JOYSTICK_RANGE = 1023;

//Pins
const int JOYSTICK_VERT = 0; // analog
const int JOYSTICK_HORIZ = 1; // analog
const int JOYSTICK_PUSH = 4; // digital

// Also connect the joystick VCC to Arduino 5V, and joystick GND to Arduino GND.

// This sketch outputs serial data at 9600 baud (open Serial Monitor to view).

void initialiseJoystick()
{
  Serial.println("Initialising joystick..."); 
  
  // make the SEL line an input
  pinMode(JOYSTICK_PUSH,INPUT);
  // turn on the pull-up resistor for the SEL line (see http://arduino.cc/en/Tutorial/DigitalPins)
  digitalWrite(JOYSTICK_PUSH,HIGH);
}

void getJoystickInput(float &x, float &y, int &push)
{
  //Serial.println("Reading joystick status..."); 
  
  x = analogRead(JOYSTICK_HORIZ)/JOYSTICK_RANGE - 0.5f;
  y = analogRead(JOYSTICK_VERT)/JOYSTICK_RANGE - 0.5f;
  push = digitalRead(JOYSTICK_PUSH);
  Serial.print("x = "); Serial.print(x, DEC); 
  Serial.print("; y = "); Serial.print(y, DEC); 
  Serial.print("; pushed = "); Serial.println((push == LOW) ? "true" : "false"); 
}

void setup()
{
  Serial.begin(9600);
  
  initialiseJoystick();
}

void loop() 
{
  float x, y;
  int push;
  
  getJoystickInput(x, y, push);
}  
