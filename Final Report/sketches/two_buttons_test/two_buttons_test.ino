const int BUTTON1 = 2;
const int BUTTON2 = 3;

void setup() {
  Serial.begin(9600);
  Serial.println("Initialising button pins...");
  pinMode(BUTTON1, INPUT);
  pinMode(BUTTON2, INPUT);
}

void loop() {
  int button1, button2;

  button1 = digitalRead(BUTTON1);
  button2 = digitalRead(BUTTON2);
  Serial.print ("Button 1: ");
  Serial.print((button1 == HIGH) ? "pressed " : "not pressed ");
  Serial.print ("Button 2: ");
  Serial.println((button2 == HIGH) ? "pressed " : "not pressed ");
}
