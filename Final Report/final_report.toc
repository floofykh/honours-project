\contentsline {section}{\numberline {1}Introduction}{2}
\contentsline {section}{\numberline {2}Literature Review}{2}
\contentsline {subsection}{\numberline {2.1}Why a Portable Gaming Console}{3}
\contentsline {subsection}{\numberline {2.2}What to Develop With}{4}
\contentsline {subsection}{\numberline {2.3}Features}{5}
\contentsline {subsubsection}{\numberline {2.3.1}Optional Features}{6}
\contentsline {subsection}{\numberline {2.4}Form and Design}{7}
\contentsline {section}{\numberline {3}Preliminary Work}{8}
\contentsline {section}{\numberline {4}Input}{14}
\contentsline {paragraph}{\nonumberline •}{14}
\contentsline {paragraph}{\nonumberline •}{16}
\contentsline {paragraph}{\nonumberline •}{16}
\contentsline {paragraph}{\nonumberline •}{16}
\contentsline {paragraph}{\nonumberline •}{18}
\contentsline {section}{\numberline {5}Device Prototype}{18}
\contentsline {paragraph}{\nonumberline •}{18}
\contentsline {paragraph}{\nonumberline •}{19}
\contentsline {section}{\numberline {6}Game}{19}
\contentsline {subsection}{\numberline {6.1}Game Design}{21}
\contentsline {subsubsection}{\numberline {6.1.1}Input}{21}
\contentsline {subsubsection}{\numberline {6.1.2}Graphics}{22}
\contentsline {subsubsection}{\numberline {6.1.3}Level Progression}{23}
\contentsline {subsection}{\numberline {6.2}Implementation}{23}
\contentsline {subsubsection}{\numberline {6.2.1}Input}{23}
\contentsline {paragraph}{\nonumberline •}{23}
\contentsline {paragraph}{\nonumberline •}{24}
\contentsline {subsubsection}{\numberline {6.2.2}Graphics}{26}
\contentsline {paragraph}{\nonumberline •}{27}
\contentsline {paragraph}{\nonumberline •}{28}
\contentsline {paragraph}{\nonumberline •}{28}
\contentsline {paragraph}{\nonumberline •}{31}
\contentsline {paragraph}{\nonumberline •}{31}
\contentsline {subsubsection}{\numberline {6.2.3}Player Update}{32}
\contentsline {paragraph}{\nonumberline •}{33}
\contentsline {paragraph}{\nonumberline Shooting}{34}
\contentsline {paragraph}{\nonumberline •}{35}
\contentsline {paragraph}{\nonumberline •}{35}
\contentsline {paragraph}{\nonumberline •}{38}
\contentsline {subsubsection}{\numberline {6.2.4}Level Progression}{38}
\contentsline {paragraph}{\nonumberline •}{39}
\contentsline {paragraph}{\nonumberline •}{41}
\contentsline {subsubsection}{\numberline {6.2.5}Collision Detection}{43}
\contentsline {subsubsection}{\numberline {6.2.6}Problems Encountered}{45}
\contentsline {paragraph}{\nonumberline •}{45}
\contentsline {paragraph}{\nonumberline •}{45}
\contentsline {paragraph}{\nonumberline •}{45}
\contentsline {section}{\numberline {7}Possible Future Work}{46}
\contentsline {paragraph}{\nonumberline •}{46}
\contentsline {paragraph}{\nonumberline •}{46}
\contentsline {section}{\numberline {8}Critical Appraisal}{46}
\contentsline {section}{\numberline {9}Conclusion}{47}
\contentsline {paragraph}{\nonumberline •}{47}
